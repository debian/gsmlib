gsmlib (1.10+20120414.gita5e5ae9a-3) unstable; urgency=medium

  * QA upload.
  * Orphan the package.

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Remove Section on gsm-utils that duplicates source.
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.

 -- Boyuan Yang <byang@debian.org>  Thu, 29 Feb 2024 13:11:37 -0500

gsmlib (1.10+20120414.gita5e5ae9a-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062182

 -- Steve Langasek <vorlon@debian.org>  Thu, 29 Feb 2024 05:31:45 +0000

gsmlib (1.10+20120414.gita5e5ae9a-2) unstable; urgency=medium

  * Refresh packaging:
  * debian/control: Bump Standards-Version to 4.6.0.
  * Apply wrap-and-sort -abst.
  * Bump debhelper compat to v13.
  * debian/rules: Modernize.
  * debian/control: Add Vcs-* fields.
  * debian/control: Make dev package depends on libc-dev, not libc6-dev.
  * debian/changelog: Remove trailing spaces.
  * debian/patches/0007-Avoid-AC_TRY_RUN: Add patch to fix FTCBFS.
    (Closes: #905958)
  * debian/patches/0008-reproducible-build: Add patch to make the
    build reproducible. (See #828066; more work needed)

 -- Boyuan Yang <byang@debian.org>  Sat, 20 Nov 2021 11:54:26 -0500

gsmlib (1.10+20120414.gita5e5ae9a-1) unstable; urgency=medium

  * Take over package maintenance via ITS process. (Closes: #996578)

 -- Boyuan Yang <byang@debian.org>  Tue, 02 Nov 2021 13:55:54 -0400

gsmlib (1.10+20120414.gita5e5ae9a-0.5) unstable; urgency=high

  * Non-maintainer upload.
  * debian/patches/0006-autoconf-Enforce-c-14.patch: Circumvent
    FTBFS caused by -std=c++17 in gcc 11.
  * debian/control: Also build-depends on autoconf-archive for
    using AX_CHECK_COMPILE_FLAG macro.

 -- Boyuan Yang <byang@debian.org>  Tue, 12 Oct 2021 13:48:24 -0400

gsmlib (1.10+20120414.gita5e5ae9a-0.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Replace obsolete Priority: extra with Priority: optional.
  * Let gsm-utils depends on lsb-base (lintian error).
  * Apply patch from Ubuntu:

  [ Lukas Märdian ]
  * Add 'external' parameter to AM_GNU_GETTEXT (Closes: #978339)

 -- Boyuan Yang <byang@debian.org>  Sun, 10 Oct 2021 13:40:35 -0400

gsmlib (1.10+20120414.gita5e5ae9a-0.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename library packages for g++ 5 transition (see #791063)

 -- Jonathan Wiltshire <jmw@debian.org>  Sat, 08 Aug 2015 14:35:16 +0100

gsmlib (1.10+20120414.gita5e5ae9a-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix build with newer gettext (Closes: #758501).
  * Update config.sub/config.guess.
  * Add an upstream patch to fix parseInt2.

 -- Andrew Shadura <andrewsh@debian.org>  Thu, 09 Oct 2014 13:24:02 +0200

gsmlib (1.10+20120414.gita5e5ae9a-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update to the latest Git version by Vianney Bouchaud.
  * Use 3.0 (quilt) source package format.
  * Own the run subdirectory (Closes: #689891).
  * Don't remove the system user on package remove.
  * Fix init script (LP: #30228).

 -- Andrew Shadura <andrewsh@debian.org>  Tue, 15 Oct 2013 13:29:27 +0200

gsmlib (1.10-13.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix +CSMS response parsing: an argument can be zero-length (Closes: #602739).
  * Fix +COPS response parsing: additional numeric argument possible
    (Closes: #674856).
  * Migrate to /run.

 -- Andrew O. Shadura <bugzilla@tut.by>  Fri, 15 Jun 2012 11:10:39 +0200

gsmlib (1.10-13.1) unstable; urgency=low

  * Non-maintainer upload.
  * Binary from dependency was used in postrm script, violating the policy. Closes: #619697.

 -- Eva Ramon Salinas <debian@empanadilla.net>  Sat, 16 Jun 2012 17:38:43 +0200

gsmlib (1.10-13) unstable; urgency=low

  * Ack NMU, Thanks Michael, Christoph & Petter
  * debian/control add Homepage:
  * Update debian/copyright; gsm-lib/COPYING actually specifies LGPL:
    - fixes lintian:copyright-without-copyright-notice
  * Update manpages fixes lintian:hyphen-used-as-minus-sign
  * Update debian/gsm-utils.init
    - fixes lintian:init.d-script-missing-lsb-short-description
  * Bug fixes from ubuntu
    - Don't install contrib/gsm-utils.init dh_installinit debian/gsm-utils.init
    - Create /var/run/gsm-utils
  * Add case 'L' to apps/gsmsmsd.cc - thks to Andrew Suffield
    - syslog support does not work (Closes: #346240)
  * gsm-utils.init really call restart with --stop first
    - init script calls --start twice (Closes: #377448)
  * Explictly set /bin/bash: gsmsmsspool & gsmsmsrequeue
    - bashism in /bin/sh script (Closes: #464981)
    - gsmsmsrequeue contains bashism or function error (Closes: #459396)
  * Patch apps/gsmsmsstore.cc - thks Isaac Wilcox
    - gsmsmsstore device existence check causes problems with RFCOMM
    devices (Closes: #340179)
  * Only start gsmsmsd if set in /etc/default/gsm-utils. crontab -> examples
    - gsmsmsd should be optional / start only if told so in
    /etc/default/gsm-utils (Closes: #474093)
  * Apply patch from Stefan Katerkamp & Jacob Nevins
    - Gsmsendsms fails with SonyEricsson W880 (fix included) (Closes:
    #413341)

 -- Mark Purcell <msp@debian.org>  Mon, 06 Oct 2008 15:01:49 +1100

gsmlib (1.10-12.5) unstable; urgency=low

  * Non-maintainer upload.
  * Yet another bashism that was later on reported on the old bug report, thus
    again closes: #464981
  * Also found a shell related problem in debian/rules and fixed it.
  * Bumped standard to 3.7.3.

 -- Michael Meskes <meskes@debian.org>  Mon, 14 Apr 2008 10:48:19 +0200

gsmlib (1.10-12.4) unstable; urgency=low

  * Non-maintainer upload.
  * Argh, somehow I mananged to upload without fixing the bug completely,
    sorry. Added those missing braces, closes: #464981.

 -- Michael Meskes <meskes@debian.org>  Wed, 09 Apr 2008 14:46:08 +0200

gsmlib (1.10-12.3) unstable; urgency=high

  * Non-maintainer upload.
  * Removed bashism in contrib/gsmsmsrequeue (Closes: #464981).

 -- Michael Meskes <meskes@debian.org>  Sun, 06 Apr 2008 15:37:35 +0200

gsmlib (1.10-12.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with GCC 4.3: 'strerror' was not declared in this scope, thanks
    to Cyril Brulebois for the patch (Closes: #455402).

 -- Christoph Berg <myon@debian.org>  Fri, 04 Apr 2008 18:01:05 +0200

gsmlib (1.10-12.1) unstable; urgency=low

  * Non-maintainer upload to solve release goal.
  * Add LSB dependency header to init.d scripts (Closes: #464061).

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 28 Mar 2008 11:39:20 +0100

gsmlib (1.10-12) unstable; urgency=low

  * addgroup --system gsmsms works better.  Thanks Jon
  * only delete gsmsms on purge
     - gsm-utils: deletes and recreates the gsmsms user on each upgrade
     (Closes: #346238)
     - gsm-utils fails installation / addgroup: The user gsmsms; does
     not exist (Closes: #445404)
  * lintian cleanup: debian-rules-ignores-make-clean-error substvar-
    source-version-is-deprecated
  * Scripts are installed +x
    - gsm-utils: uselessly installs non-executable scripts into /usr/bin
    (Closes: #346230)
  * Remove bogus symlink
    - gsm-utils: wrong symlink for manpage gsmsiectl.1 (Closes: #322382)
    - gsm-utils: gsmsiectl.1 dangling symlink (Closes: #399582)
  * debian/gsm-utils.init reload/restart was not calling --stop. Thanks
    Barry
    - init script calls --start twice (Closes: #377448)

 -- Mark Purcell <msp@debian.org>  Mon, 08 Oct 2007 21:44:00 +0100

gsmlib (1.10-11) unstable; urgency=low

  * Create system group gsmsms - Thanks Emmanuel
     - gsm-utils: creates group in non-system gid range (Closes: #353967)
     - gsm-utils: postinst should create system grp gsmsms (Closes:
    #390266)
  * Upgrade to compat 4
  * Apply gcc-4.3 patch from Martin
    - FTBFS with GCC 4.3: missing #includes (Closes: #417222)

 -- Mark Purcell <msp@debian.org>  Sat, 29 Sep 2007 18:22:56 +0100

gsmlib (1.10-10) unstable; urgency=low

  * FTBFS with G++ 4.1: extra qualifications (Closes: #356109)

 -- Mark Purcell <msp@debian.org>  Sat, 20 May 2006 21:54:42 +0100

gsmlib (1.10-9) unstable; urgency=low

  * library package needs to be renamed (libstdc++ allocator change)
    (Closes: #339179)

 -- Mark Purcell <msp@debian.org>  Mon, 21 Nov 2005 21:19:51 +0000

gsmlib (1.10-8) unstable; urgency=low

  * removal of automake1.6 (Closes: #335123)
  * fails with dash [bashisms in scripts] (Closes: #309834)
  * Update libtool Fixes: gsmlib(GNU/k*BSD): FTBFS: out of date libtool scripts (Closes:
    #319688)
  * [INTL:de] German PO file corrections (Closes: #314060)
  * Fix: old-fsf-address-in-copyright-file

 -- Mark Purcell <msp@debian.org>  Thu,  3 Nov 2005 22:40:19 +0000

gsmlib (1.10-7) unstable; urgency=low

  * C++ 4.0 transition
  * Closes: #315864: Missing manpages
  * gsm-utils: maintainer-script-needs-depends-on-adduser postinst

 -- Mark Purcell <msp@debian.org>  Sat, 23 Jul 2005 00:46:31 +1000

gsmlib (1.10-6) unstable; urgency=low

  * Rebuild for invalid dependancies
  * Closes: #258056: libgsmme 99% cpu usage
    - Patch from Emard
  * Closes: #274382: FTBFS with gcc-3.4: template-id `operator&lt;
    &lt;&gt;' for `bool gsmlib::operator&lt;(const
    gsmlib::MapKey&lt;gsmlib::SortedPhonebookBase&gt;&amp;, const
    gsmlib::MapKey&lt;gsmlib::SortedPhonebookBase&gt;&amp;)' does not
    match any template declaration
    - Patch from Andreas Jochens
  * Closes: #294251: FTBFS (amd64/gcc-4.0): explicit qualification in
    declaration of `bool gsmlib::operator&lt;(const
    gsmlib::MapKey&lt;SortedStore&gt;&amp;, const
    gsmlib::MapKey&lt;SortedStore&gt;&amp;)'
    - Patch from Andreas Jochens
  * Closes: #200189: Patch and contribution
    +  Added multi-queue-priority-system and syslog patch (Matthias Goebl)
    +  Included init, spool and requeue scripts for gsmsmsd (Matthias Goebl)
    +  gsmsmsd runs with own user and group (gsmsms:gsmsms) (Matthias Goebl)

 -- Mark Purcell <msp@debian.org>  Tue, 17 May 2005 11:34:45 +0100

gsmlib (1.10-5) unstable; urgency=low

  * Change Section: libdevel
  * gsm_unix_serial.cc patch from Daniel Schepler to fix g++-3.3
    compliation. Thanks. (Closes: Bug#195151)

 -- Mark Purcell <msp@debian.org>  Sat, 19 Jul 2003 15:57:28 +1000

gsmlib (1.10-4) unstable; urgency=low

  * Include file descriptor leak patch from Edd Dumbill (Closes:
    Bug#168475)
  * lintian cleanup: description-synopsis-might-not-be-phrased-properly
  * lintian cleanup: configure-generated-file-in-source

 -- Mark Purcell <msp@debian.org>  Sun,  9 Feb 2003 14:04:54 +1100

gsmlib (1.10-3) unstable; urgency=low

  * New Maintainer (Closes: Bug#180061). Thanks Mikael for your work.

 -- Mark Purcell <msp@debian.org>  Sat,  8 Feb 2003 16:55:26 +1100

gsmlib (1.10-2) unstable; urgency=low

  * Rebuild to use the new c++ ABI (GCC 3.2)

 -- Mikael Hedin <micce@debian.org>  Thu, 23 Jan 2003 20:57:50 +0100

gsmlib (1.10-1) unstable; urgency=low

  * New upstrem release.

 -- Mikael Hedin <micce@debian.org>  Wed,  6 Nov 2002 17:44:17 +0100

gsmlib (1.9-2) unstable; urgency=low

  * Made new rules for the config.guess/sub update thing (closes: #146865,
    #146867).

 -- Mikael Hedin <micce@debian.org>  Tue, 14 May 2002 09:28:03 +0200

gsmlib (1.9-1) unstable; urgency=low

  * New upstream version.
  * Use chrpath to get rid of rpaths.
  * Add mini-manpage for gsmsiexfer.
  * Remove b-d on auto-stuff, we don't use them.

 -- Mikael Hedin <micce@debian.org>  Mon, 13 May 2002 22:10:28 +0200

gsmlib (1.8-2) unstable; urgency=low

  * Removed b-d on gcc 3.0, as they are no longer nessecary.

 -- Mikael Hedin <micce@debian.org>  Thu, 24 Jan 2002 12:59:07 +0100

gsmlib (1.8-1) unstable; urgency=low

  * New upstream version.
  * Revert the arch hack, now it should compile with either g++.
  * Include the new lib in libgsmme1.  Run dh_makeshlibs -V because of this.
  * Added info for gsmsiectl in gsmctl(1).

 -- Mikael Hedin <micce@debian.org>  Wed,  9 Jan 2002 22:38:45 +0100

gsmlib (1.7-2) unstable; urgency=low

  * gsm-utils: Added shlibs:Depends (closes: #126127).
  * Spelling correction (closes: #124705, #124972)
  * Rm libgsmme1.postins, and let dh_makeshlibs take care of ldconfig.
  * Made explicit arch list without sparc and arm, they cannot use g++-3.0
    right now.

 -- Mikael Hedin <micce@debian.org>  Sat, 22 Dec 2001 20:27:54 +0100

gsmlib (1.7-1) unstable; urgency=low

  * New upstream
  * Use gcc-3.0 and g++-3.0, 2.95 doesn't compile.

 -- Mikael Hedin <micce@debian.org>  Thu,  1 Nov 2001 10:24:33 +0100

gsmlib (1.6-5) unstable; urgency=low

  * Updated manpage (closes: #110973)
  * Corrected problem with OP status (closes: #110970)

 -- Mikael Hedin <micce@debian.org>  Sat,  8 Sep 2001 18:12:17 +0200

gsmlib (1.6-4) unstable; urgency=low

  * Support DEB_BUILD_OPTIONS
  * Changed libgsmme-dev to section devel.
  * Reran libtoolize.
  * Lots of small patches to compile with g++-3.0. (Closes: #104411)
  * Removed dh_testversion.

 -- Mikael Hedin <micce@debian.org>  Thu, 12 Jul 2001 16:06:23 +0200

gsmlib (1.6-3) unstable; urgency=low

  * Various minor corrections.

 -- Mikael Hedin <mikael.hedin@irf.se>  Thu,  8 Mar 2001 16:24:07 +0100

gsmlib (1.6-2) unstable; urgency=low

  * Dont install INSTALL.  Correct indentation for libgsmme1 description.

 -- Mikael Hedin <mikael.hedin@irf.se>  Tue,  6 Mar 2001 14:55:05 +0100

gsmlib (1.6-1) unstable; urgency=low

  * New upstream version.

 -- Mikael Hedin <mikael.hedin@irf.se>  Mon, 29 Jan 2001 17:57:21 +0100

gsmlib (1.5-1) unstable; urgency=low

  * Initial Release.

 -- Mikael Hedin <mikael.hedin@irf.se>  Thu, 14 Dec 2000 01:06:40 +0100
